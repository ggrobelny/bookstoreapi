﻿//using BStore.Controllers.Models;
using BookStore.Data.Interfaces;
using BookStore.Data.Repositories;
//using BStore.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BStore.Data.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        //public List<Book> books = new List<Book>()
        //{
        //    new Book { Id = 1, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 2, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 3, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 4, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 5, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 6, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 7, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 8, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
        //    new Book { Id = 9, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false }
        //};

        private IBookRepository books = new BookRepository();

        [HttpGet]
        public ActionResult<IEnumerable<Book>> GetAllBooks()
        {
            return books;
        }

        [HttpGet("{id}")]
        public ActionResult<Book> GetBook(int id)
        {
            var book = books.FirstOrDefault(x => x.Id == id);
            if (book == null)
            {
                return NotFound();
            }
            return book;
        }

    }
}
